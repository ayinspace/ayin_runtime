package space.ayin.runtime.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import space.ayin.lang.ayin.AyinStruct;
import space.ayin.runtime.runtimemodel.*;
import space.ayin.runtime.utils.*;

public class Testik {
	
	private static PluginDescriptor createPluginDescriptor(String instanceId, String implClass, String pluginId) {
		ClassPathPluginProvider pluginProvider = AyinRuntimeModelFactory.eINSTANCE.createClassPathPluginProvider();
		pluginProvider.setImplClass(implClass);
		pluginProvider.setPluginId(pluginId);
		PluginDescriptor pluginDescriptor = AyinRuntimeModelFactory.eINSTANCE.createPluginDescriptor();
		pluginDescriptor.setInstanceId(instanceId);
		pluginDescriptor.setPluginProvider(pluginProvider);
		return pluginDescriptor;
	}
	
	private static StringProperty createStringProperty(String value) {
		StringProperty stringProperty = AyinRuntimeModelFactory.eINSTANCE.createStringProperty();
		stringProperty.setValue(value);
		return stringProperty;
	}

	
	private static NumberProperty createNumberProperty(int value) {
		NumberProperty NumberProperty = AyinRuntimeModelFactory.eINSTANCE.createNumberProperty();
		NumberProperty.setValue(BigDecimal.valueOf(value));
		return NumberProperty;
	}
	
	private static ListProperty createListProperty(AyinExecutorProperty... properties) {
		ListProperty listProperty = AyinRuntimeModelFactory.eINSTANCE.createListProperty();
		for (int i = 0; i < properties.length; i++) {
			listProperty.getProperties().add(properties[i]);
		}
		return listProperty;
	}
	
	private static RefProperty createRefProperty(PluginDescriptor pluginDescriptor) {
		RefProperty refProperty = AyinRuntimeModelFactory.eINSTANCE.createRefProperty();
		refProperty.setPlugin(pluginDescriptor);
		return refProperty;
	}
	
	private static PluginProperty createPluginProperty(String pluginId) {
		PluginProperty pluginProperty = AyinRuntimeModelFactory.eINSTANCE.createPluginProperty();
		pluginProperty.setPluginId(pluginId);
		return pluginProperty;
	}
	
	
	public static void main(String[] args) {

		/*AyinStructObject struct = AyinRuntimeModelFactory.eINSTANCE.createAyinStructObject();
		AyinPacket structPacket = AyinRuntimeUtils.pack(struct);
		System.out.println(AyinRuntimeUtils.serializeToXml(structPacket));*/
		
		
		/*
		List<EObject> l = new ArrayList<>();
		l.add(AyinRuntimeModelFactory.eINSTANCE.createAyinStructObject());
		l.add(AyinRuntimeModelFactory.eINSTANCE.createAyinStructObject());
		l.add(AyinRuntimeModelFactory.eINSTANCE.createAyinStructObject());

		AyinListPacket packet = AyinRuntimeUtils.pack(l);
		System.out.println(AyinRuntimeUtils.serializeToXml(packet));
		*/
		
		ExecutorProfile executorProfile = AyinRuntimeModelFactory.eINSTANCE.createExecutorProfile();
		
		PluginDescriptor loggerPluginDescriptor = createPluginDescriptor("logger1", "space.ayin.executor.plugins.AyinLoggerPlugin", "space.ayin.executor.plugins.logger");
		
		ExecutorDescriptor executorDescriptor = AyinRuntimeModelFactory.eINSTANCE.createExecutorDescriptor();
		executorDescriptor.setIgnoreUnusedProperties(true);
		
		executorProfile.getDescriptors().add(executorDescriptor);
		
		
		executorDescriptor.getPlugins().add(loggerPluginDescriptor);

		
		loggerPluginDescriptor.getProperties().put("childLogger", createRefProperty(loggerPluginDescriptor));
		
		
		loggerPluginDescriptor.getProperties().put("prefix", createStringProperty("aaaa"));
		
		loggerPluginDescriptor.getProperties().put("anotherLogger", createPluginProperty("space.ayin.executor.plugin.logger"));
		
		MapProperty mapProperty = AyinRuntimeModelFactory.eINSTANCE.createMapProperty();
		
		
		mapProperty.getProperties().put("a", createStringProperty("11"));
		mapProperty.getProperties().put("b", createStringProperty("22"));
		
		loggerPluginDescriptor.getProperties().put("other", mapProperty);
		
		loggerPluginDescriptor.getProperties().put("someList", createListProperty(createNumberProperty(11), createNumberProperty(22)));
		
		
		executorDescriptor.getPlugins().add(loggerPluginDescriptor);
		
		System.out.println(AyinRuntimeUtils.serializeToXml(executorProfile));
		
		
	}
}
