package space.ayin.runtime.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.BinaryResourceImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

import space.ayin.runtime.runtimemodel.AyinCallRequest;
import space.ayin.runtime.runtimemodel.AyinCallResponse;
import space.ayin.runtime.runtimemodel.AyinEObjectPacket;
import space.ayin.runtime.runtimemodel.AyinInvoke;
import space.ayin.runtime.runtimemodel.AyinInvokeResult;
import space.ayin.runtime.runtimemodel.AyinListPacket;
import space.ayin.runtime.runtimemodel.AyinPacket;
import space.ayin.runtime.runtimemodel.AyinRuntimeModelFactory;


public class AyinRuntimeUtils {

	
	private static void pack(EObject toPack, AyinPacket packet) {
		try {
			Set<EObject> alreadyProcessed = new HashSet<>();
			addToHeap(packet, toPack, alreadyProcessed);
			packet.getHeap().addAll(alreadyProcessed);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static void pack(List<EObject> toPack, AyinPacket packet) {
		Set<EObject> alreadyProcessed = new HashSet<>();
		for (Object o : toPack) {
			addToHeap(packet, (EObject) o, alreadyProcessed);
		}
		packet.getHeap().addAll(alreadyProcessed);
	}

	
	public static AyinEObjectPacket pack(EObject struct) {
		EObject structCopy = EcoreUtil.copy(struct);
		AyinEObjectPacket packet = AyinRuntimeModelFactory.eINSTANCE.createAyinEObjectPacket();
		pack(structCopy, packet);
		packet.setRoot(structCopy);
		return packet;
	}
	
	public static AyinListPacket pack(List<EObject> list) {
		List listCopy = new ArrayList(EcoreUtil.copyAll(list));
		AyinListPacket packet = AyinRuntimeModelFactory.eINSTANCE.createAyinListPacket();
		pack(listCopy, packet);
		packet.getRoot().addAll(listCopy);
		return packet;
	}
	
	public static AyinCallRequest packToCallMessage(AyinInvoke ayinInvoke) {
		AyinInvoke ayinInvokeCopy = EcoreUtil.copy(ayinInvoke);
		AyinCallRequest callMessage = AyinRuntimeModelFactory.eINSTANCE.createAyinCallRequest();
		pack(ayinInvokeCopy, callMessage);
		callMessage.setInvoke(ayinInvokeCopy);
		return callMessage;
	}
	
	public static AyinCallResponse packToCallResponseMessage(AyinInvokeResult ayinInvokeResult) {
		AyinInvokeResult ayinInvokeResultCopy = EcoreUtil.copy(ayinInvokeResult);
		AyinCallResponse callResponse = AyinRuntimeModelFactory.eINSTANCE.createAyinCallResponse();
		pack(ayinInvokeResultCopy, callResponse);
		callResponse.setInvokeReturn(ayinInvokeResultCopy);
		return callResponse;
	}

    private static void addToHeap(AyinPacket message, EObject eObject, Set<EObject> alreadyProcessed) {
        if (alreadyProcessed.contains(eObject)) {
            return;
        }
        alreadyProcessed.add(eObject);
        for (EReference eReference : eObject.eClass().getEAllReferences()) {
            if (!eReference.isContainment() && eReference.getUpperBound() != -1) {
                EObject o = (EObject) eObject.eGet(eReference);
                if (o != null) {
                    addToHeap(message, o, alreadyProcessed);
                }
            } else if (eReference.getUpperBound() == -1) {
                List<EObject> o = (List<EObject>) eObject.eGet(eReference);

                for (EObject eobj : o) {
                    addToHeap(message, eobj, alreadyProcessed);
                }
            }
        }
    }

	public static String serializeToXml(EObject eObject) {
		XMLResourceImpl xmlResource = new XMLResourceImpl();

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		xmlResource.getContents().add(eObject);

		try {
			xmlResource.save(byteArrayOutputStream, Collections.EMPTY_MAP);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return new String(byteArrayOutputStream.toByteArray());

	}

	public static EObject deserializeFromXml(String xml) {
		Resource xmlResource = new XMLResourceImpl();
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(xml.getBytes());
		try {
			xmlResource.load(byteArrayInputStream, Collections.EMPTY_MAP);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return xmlResource.getContents().get(0);
	}

	public static byte[] serializeToBinary(EObject eObject) {
		Resource resource = new BinaryResourceImpl();

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		resource.getContents().add(eObject);

		try {
			resource.save(byteArrayOutputStream, Collections.EMPTY_MAP);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return byteArrayOutputStream.toByteArray();

	}

	public static EObject deserializeFromBinary(byte[] data) {
		Resource resource = new BinaryResourceImpl();
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
		try {
			resource.load(byteArrayInputStream, Collections.EMPTY_MAP);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return resource.getContents().get(0);
	}

}
